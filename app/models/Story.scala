package models

import play.api.libs.json.{Writes, JsPath, Reads}
import play.api.libs.functional.syntax._
import reactivemongo.bson.BSONObjectID
import play.modules.reactivemongo.json.BSONFormats._

/**
 * Created by imu on 10/30/2014.
 */
case class Story(id: Option[BSONObjectID], title: String, sprintId: String) {

}

object Story {

  import play.api.libs.json.Json

  //thanks to Json macros
  implicit val storyFormat = Json.format[Story]

  //the boring way
//    implicit val storyReads: Reads[Story] = (
//      (JsPath \ "id").read[Option[BSONObjectID]] and
//        (JsPath \ "title").read[String] and
//        (JsPath \ "sprintId").read[String]
//      )(Story.apply _)
//
//    implicit val storyWrites: Writes[Story] = (
//      (JsPath \ "id").write[Option[BSONObjectID]] and
//        (JsPath \ "title").write[String] and
//        (JsPath \ "sprintId").write[String]
//      )(unlift(Story.unapply _))
}
