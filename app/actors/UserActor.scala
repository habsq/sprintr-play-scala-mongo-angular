package actors

import akka.actor.{Props, Actor, ActorRef}
import play.api.libs.json.Json

/**
 * Created by imu on 10/31/2014.
 */
class UserActor(out: ActorRef) extends Actor {

  CaptureActor.captureActor ! new WatchSprint("whatever")

  def receive: Receive = {
    case SprintUpdate(stories) => {
      println("got an update " + Json.arr(stories).toString())
      out ! Json.toJson(stories).toString()
    }
    //case s: String => out ! """[{"title":"wohoo", "sprintId":"0"},{"title":"haiii", "sprintId":"0"}]"""
  }

}

object UserActor {
  def props(out: ActorRef) = Props(new UserActor(out))
}