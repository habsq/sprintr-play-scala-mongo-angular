package actors

import akka.actor.{Props, ActorRef, Actor}
import akka.actor.Actor.Receive
import models.Story
import play.libs.Akka

import scala.collection.immutable.HashSet

/**
 * Created by imu on 10/31/2014.
 */
class CaptureActor extends Actor{

  var watchers: HashSet[ActorRef] = HashSet.empty[ActorRef]

  override def receive: Receive = {
    case WatchSprint(_) => {
      println("new subscriber")
      watchers = watchers + sender
      println(watchers)
    }
    case SprintUpdate(stories) => {
      println("update: " + stories)
      watchers.foreach(_ ! new SprintUpdate(stories))
    }
  }
}

object CaptureActor {
  lazy val captureActor: ActorRef = Akka.system.actorOf(Props(classOf[CaptureActor]))
}

case class WatchSprint(sprintName:String)

case class SprintUpdate(stories:List[Story])
