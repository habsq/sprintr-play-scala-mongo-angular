package controllers

import play.api._
import play.api.mvc._
import play.modules.reactivemongo.MongoController

object Application extends Controller{

  def index = Action {
    Ok(views.html.indexsimple())
  }

}