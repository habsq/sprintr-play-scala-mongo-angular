package controllers

import actors.{CaptureActor, SprintUpdate, UserActor}
import akka.actor.Props
import models.Story
import play.api.Logger
import play.api.libs.iteratee.{Iteratee, Concurrent}
import play.api.libs.json.{JsArray, JsObject, Json}
import play.api.mvc.{WebSocket, Action, Controller}
import play.modules.reactivemongo.MongoController
import play.modules.reactivemongo.json.collection.JSONCollection
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import reactivemongo.api.Cursor
import play.api.Play.current

import scala.concurrent.Future

/**
 * Created by imu on 10/30/2014.
 */
object Capture extends Controller with MongoController {

  def collection: JSONCollection = db.collection[JSONCollection]("stories")

  def connect = WebSocket.acceptWithActor[String, String] { request => out =>
    UserActor.props(out)
  }

  def create = Action.async(parse.json) {
    request =>
      request.body.validate[Story].map { story =>
        // `story` is an instance of the case class `models.Story`
        collection.insert(story).map { lastError =>
          Logger.debug(s"Successfully inserted with LastError: $lastError")

          //dirty codes here
          // let's do our query
          val cursor: Cursor[Story] = collection.find(Json.obj()).
            // perform the query and get a cursor of JsObject
            cursor[Story]

          // gather all the JsObjects in a list
          val futureStoryList: Future[List[Story]] = cursor.collect[List]()

          futureStoryList.map{
            stories => {
              CaptureActor.captureActor!new SprintUpdate(stories)
            }
          }
          //--end

          Created
        }
      }.getOrElse(Future.successful(BadRequest("invalid json")))
  }

  def findStory() = Action.async {

    // let's do our query
    val cursor: Cursor[Story] = collection.find(Json.obj()).
      // perform the query and get a cursor of JsObject
      cursor[Story]

    // gather all the JsObjects in a list
    val futureStoryList: Future[List[Story]] = cursor.collect[List]()

    //transform to JsObjects
    val futureStoryJsonArray: Future[JsArray] = futureStoryList.map { stories =>
      Json.arr(stories)
    }

    futureStoryJsonArray.map { stories =>
      Ok(stories(0))
    }
  }

  def findStoryById(id: String) =
    Action.async {
      val futureStory: Future[Option[Story]] = collection.find(Json.obj("_id" -> id)).one[Story]

      futureStory.map {
        case Some(story) => Ok(Json.toJson(story))
        case None => NotFound(Json.obj("message" -> "No such item"))
      }
    }
}
