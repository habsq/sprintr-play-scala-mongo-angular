/*global require, requirejs */

'use strict';

requirejs.config({
  paths: {
    'angular': ['https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.0/angular'],
    'angular-route': ['https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.0/angular-route']
  },
  shim: {
    'angular': {
      exports : 'angular'
    },
    'angular-route': {
      deps: ['angular'],
      exports : 'angular'
    }
  }
});

require(['angular', './controllers', './directives', './filters', './services', 'angular-route'],
  function(angular, controllers) {

    // Declare app level module which depends on filters, and services

    angular.module('myApp', ['myApp.filters', 'myApp.services', 'myApp.directives', 'ngRoute']).
      config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/capture', {templateUrl: 'partials/capture.html', controller: controllers.MyCtrl1});
        $routeProvider.when('/develop', {templateUrl: 'partials/develop.html', controller: controllers.MyCtrl2});
        $routeProvider.otherwise({redirectTo: '/capture'});
      }]);

    angular.bootstrap(document, ['myApp']);

});
