angular.module("myapp", [])
    .controller("HelloController", function ($scope, $http, $timeout, $rootScope) {
        $scope.helloTo = {};
        $scope.helloTo.title = "World, AngularJS";
        $scope.myData = {};
        //$scope.myData.fromServer = [];
        $scope.myData.doClick = function (item, event) {

//            var responsePromise = $http.get("/stories");
//
//            responsePromise.success(function (data, status, headers, config) {
//                $scope.myData.fromServer = data;
//            });
//            responsePromise.error(function (data, status, headers, config) {
//                alert("AJAX failed!");
//            });


            $scope.socket.send("Hai");
        }

        $scope.socket = new WebSocket("ws://localhost:9000/stories/connect");

        $scope.socket.onopen = function () {
            console.log("Socket is opened");
        }

        $scope.socket.onmessage = function (msg) {
            console.log("Receiving message:");
            var result = angular.fromJson(msg.data);
            console.log(result);
            $scope.myData.fromServer = result;
            $scope.$apply();
        }
    });